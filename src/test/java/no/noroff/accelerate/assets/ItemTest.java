package no.noroff.accelerate.assets;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void getA() {
        Item i = new Item(5);

        assertEquals(5, i.getA());
    }
}