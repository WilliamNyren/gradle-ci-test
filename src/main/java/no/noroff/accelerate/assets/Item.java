package no.noroff.accelerate.assets;

public class Item {
    private int a;

    public Item(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }
}
